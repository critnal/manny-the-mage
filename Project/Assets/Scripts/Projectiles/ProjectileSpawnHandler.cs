﻿using UnityEngine;
using System.Collections;

public class ProjectileSpawnHandler
{
    public GameObject GmObject { get; protected set; }
    public float SpawnWeight { get; protected set; }
    public float Damage { get; protected set; }

    public ProjectileSpawnHandler(GameObject gmObject)
    {
        this.GmObject = gmObject;
        Projectile projectile = gmObject.GetComponent<Projectile>();
        this.SpawnWeight = projectile.SpawnWeight;
        this.Damage = projectile.Damage;
    }
}

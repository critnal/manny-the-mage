﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Arrow : Projectile
{
    public override float Damage
    {
        get { return Data.Projectile.Arrow.Damage; }
    }
    public override float SpawnWeight
    {
        get { return Data.Projectile.Arrow.SpawnWeight; }
    }
    protected bool hasHitAnEnemy { get; set; }


	protected void Awake()
	{
		MoveSpeed = Data.Projectile.Arrow.MoveSpeed;
        hasHitAnEnemy = false;
	}

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == Data.Tag.DespawnZone)
        {
            DetachVisualEffects();
            ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
            Destroy(this.gameObject);
        }
        else if (other.tag == Data.Tag.Enemy)
        {
            Enemy enemy = other.GetComponent<Enemy>();
            if (enemy.ID == TargetID)
            {
                enemy.TakeDamage(this);
                DetachVisualEffects();
                ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
                Destroy(this.gameObject);
            } 
        }
        else if (other.tag == Data.Tag.Base)
        {
            BaseHandler.Instance.TakeDamage(Damage);
            ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
            DetachVisualEffects();
            Destroy(this.gameObject);
        }
    }
}

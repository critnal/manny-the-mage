﻿using UnityEngine;
using System.Collections;

public class Explosion : Projectile {

    public override float Damage 
    {
        get { return Data.Projectile.Explosion.Damage; }
    }
    public override float SpawnWeight
    {
        get { return Data.Projectile.Fireball.SpawnWeight; }
    }
	private float triggerEndTime = 0f;
	private float visualEndTime = 0f;
	

    protected void Awake()
    {
        triggerEndTime = Time.time + Data.Projectile.Explosion.TriggerDuration;
        visualEndTime = Time.time + Data.Projectile.Explosion.VisualDuration;
    }
	
	private void Update()
	{
		if (Time.time > triggerEndTime)
		{
			this.collider.enabled = false;
		}
		if (Time.time > visualEndTime)
		{
			Destroy(this.gameObject);
		}
	}

	protected override void OnTriggerEnter(Collider other)
	{
		if (other.tag == Data.Tag.Enemy)
		{
            other.GetComponent<Enemy>().TakeDamage(this);
		}
	}
}

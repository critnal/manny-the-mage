﻿using UnityEngine;
using System.Collections;

public class Boulder : Projectile
{
    public override float Damage
    {
        get { return Data.Projectile.Boulder.Damage; }
    }
    public override float SpawnWeight
    {
        get { return Data.Projectile.Boulder.SpawnWeight; }
    }
	public float DeccelerationSpeed = 15f;
	public float InitialGroundSpeedReduction = 0.6f;
	protected bool isGrounded = false;
    protected Material groundedTexture;
	

	private void Awake()
	{
		MoveSpeed = Data.Projectile.Boulder.MoveSpeed;
        groundedTexture = (Material)Resources.Load("Resources/Visuals/Materials/boulder grounded sprite");
	}
	
	private void Update()
	{
		if (isGrounded && MoveSpeed <= 0f)
		{			
			DetachVisualEffects();
			Destroy(this.gameObject);
		}
	}
	
	private void FixedUpdate()
	{
		if (isGrounded)
		{
			MoveSpeed -= DeccelerationSpeed * Time.deltaTime;
//			print(MoveSpeed);
			Vector3 direction = this.rigidbody.velocity.normalized;
			this.rigidbody.velocity = direction * MoveSpeed;
		}
	}
	
	protected override void OnTriggerEnter(Collider other)
	{
		if (other.tag == Data.Tag.DespawnZone)
		{
			DetachVisualEffects();
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			Destroy(this.gameObject);
		}
		else if (other.tag == Data.Tag.Enemy)
        {
            Enemy enemy = other.GetComponent<Enemy>();
            if (isGrounded || enemy.ID == TargetID)
            {
                enemy.TakeDamage(this);
                MoveSpeed *= InitialGroundSpeedReduction;
                MoveInDirection(this.rigidbody.velocity.normalized);
                //			print(Vector3.Magnitude(this.rigidbody.velocity));
                isGrounded = true;
                //SetGroundedTexture();
                ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
            }			
		}
		else if (other.tag == Data.Tag.Base)
		{			
			BaseHandler.Instance.TakeDamage(Damage);
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			DetachVisualEffects();
			Destroy(this.gameObject);
		}
	}

    protected void SetGroundedTexture()
    {
        this.renderer.material = groundedTexture;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Projectile : MonoBehaviour
{	
	public int ID {	get; set; }
	public float MoveSpeed { get; set; }
	public abstract float Damage { get; }
    public abstract float SpawnWeight { get; }
    public int TargetID { get; set; }

	
	public void MoveInDirection(Vector3 direction)
	{
		this.rigidbody.velocity = direction.normalized * MoveSpeed;
        this.transform.rotation = Quaternion.LookRotation(direction, Vector3.forward);
	}
	
	protected virtual void OnTriggerEnter(Collider other)
	{
		if (other.tag == Data.Tag.DespawnZone)
		{
			DetachVisualEffects();
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			Destroy(this.gameObject);
		}
		else if (other.tag == Data.Tag.Enemy)
		{
            Enemy enemy = other.GetComponent<Enemy>();
            if (enemy.ID == TargetID)
            {
                enemy.TakeDamage(this);
                DetachVisualEffects();
                ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
                Destroy(this.gameObject);
            }            
		}
		else if (other.tag == Data.Tag.Base)
		{			
			BaseHandler.Instance.TakeDamage(Damage);
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			DetachVisualEffects();
			Destroy(this.gameObject);
		}
	}
	
	public void Select()
	{
		this.rigidbody.velocity = Vector3.zero;
	}
	
	public void Release(Vector3 direction, float speed)
	{
		this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, Data.PositionLayer.Enemy);
		MoveSpeed = speed;
		MoveInDirection(direction);
		return;
	}
	
	protected void DetachVisualEffects()
	{
		foreach (ParticleSystem system in this.GetComponentsInChildren<ParticleSystem>()) 
		{
			system.enableEmission = false;
			system.transform.parent = null;
			Destroy(system.gameObject, 2f);
		}		
		foreach (TrailRenderer renderer in this.GetComponentsInChildren<TrailRenderer>()) 
		{
			renderer.transform.parent = null;
			Destroy(renderer.gameObject, 2f);
		}		
	}
}

﻿using UnityEngine;
using System.Collections;

public class Fireball : Projectile
{
    public override float Damage
    {
        get { return Data.Projectile.Fireball.Damage; }
    }
    public override float SpawnWeight
    {
        get { return Data.Projectile.Fireball.SpawnWeight; }
    }
	private GameObject explosionGmObject;


	private void Awake()
	{
		explosionGmObject = (GameObject)Resources.Load("Prefabs/Explosion");
		MoveSpeed = Data.Projectile.Fireball.MoveSpeed;
	}
	
	protected override void OnTriggerEnter(Collider other)
	{
		if (other.tag == Data.Tag.DespawnZone)
		{
			DetachVisualEffects();
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			Destroy(this.gameObject);
		}
		else if (other.tag == Data.Tag.Enemy)
		{
            if (other.GetComponent<Enemy>().ID == TargetID)
            {
                Explosion explosion = ((GameObject)Instantiate(explosionGmObject, this.transform.position,
                Quaternion.Euler(new Vector3(90f, 0f, 0f)))).GetComponent<Explosion>();
                explosion.ID = this.ID;
                DetachVisualEffects();
                ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
                Destroy(this.gameObject);
            }
		}
		else if (other.tag == Data.Tag.Base)
		{			
			BaseHandler.Instance.TakeDamage(Damage);
			ProjectileHandler.Instance.ActiveProjectiles.Remove(this);
			DetachVisualEffects();
			Destroy(this.gameObject);
		}
	}
}

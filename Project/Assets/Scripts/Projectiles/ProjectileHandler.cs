﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProjectileHandler : Singleton<ProjectileHandler> 
{
	protected ProjectileHandler() {}

    private List<ProjectileSpawnHandler> projectileSpawnHandlers = new List<ProjectileSpawnHandler>();
	public List<Projectile> ActiveProjectiles = new List<Projectile>();
	private float nextProjectileSpawnTime = 0f;
    private int currentProjectileID = 0;
    private List<EnemyGroup> potentialEnemyGroups = new List<EnemyGroup>();
	
	private void Start()
    {
        projectileSpawnHandlers.Add(new ProjectileSpawnHandler(
            Data.Projectile.Arrow.GmObject));
        projectileSpawnHandlers.Add(new ProjectileSpawnHandler(
            Data.Projectile.Fireball.GmObject));
        projectileSpawnHandlers.Add(new ProjectileSpawnHandler(
            Data.Projectile.Boulder.GmObject));	
	}
	
	private void Update()
    {
        if (Time.time > nextProjectileSpawnTime && EnemyHandler.Instance.EnemyGroups.Count > 0)            
		{
            GetPotentialEnemyGroups();
            if (potentialEnemyGroups.Count > 0)
            {
                EnemyGroup enemyGroup = GetRandomEnemyGroupFromList(potentialEnemyGroups);
                int randomTargetCount = Random.Range(0, enemyGroup.Enemies.Count);
                if (randomTargetCount == 0 || enemyGroup.Enemies.Count == 1)
                {
                    TargetIndividual(GetRandomEnemyFromGroup(enemyGroup));
                }
                else
                {
                    TargetGroup(enemyGroup);
                }
                nextProjectileSpawnTime = Time.time + Data.Projectile.Spawn.GetWaitTime();
            }
		}
    }

    private void GetPotentialEnemyGroups()
    {
        potentialEnemyGroups.Clear();
        foreach (EnemyGroup enemyGroup in EnemyHandler.Instance.EnemyGroups)
        {
            if (enemyGroup.HasPotentialEnemies())
            {
                potentialEnemyGroups.Add(enemyGroup);
            }
        }
    }

    private EnemyGroup GetRandomEnemyGroupFromList(List<EnemyGroup> enemyGroups)
    {
        EnemyGroup currentEnemyGroup;
        do
        {
            currentEnemyGroup = enemyGroups[Random.Range(0, enemyGroups.Count)];
        }
        while (!currentEnemyGroup.HasPotentialEnemies());
        return currentEnemyGroup;
    }

    private Enemy GetRandomEnemyFromGroup(EnemyGroup enemyGroup)
    {
        return enemyGroup.GetPotentialEnemies()[Random.Range(0, enemyGroup.Enemies.Count)];
    }

    private void TargetIndividual(Enemy enemy)
    {
        ProjectileSpawnHandler projectileSpawnHandler = GetRandomProjectileToSpawn();
        Projectile spawnedProjectile = SpawnProjectile(projectileSpawnHandler);
        enemy.TakePotentialDamage(spawnedProjectile.Damage);
    }

    private void TargetGroup(EnemyGroup enemyGroup)
    {
        ProjectileSpawnHandler projectileSpawnHandler = GetRandomGroupProjectileToSpawn();
        Projectile spawnedProjectile = SpawnProjectile(projectileSpawnHandler);
        for (int i = enemyGroup.Enemies.Count - 1; i >= 0; i--)
        {
            enemyGroup.Enemies[i].TakePotentialDamage(spawnedProjectile.Damage);
        }
    }

    private ProjectileSpawnHandler GetRandomProjectileToSpawn()
    {
        float totalSpawnWeight = 0f;
        foreach (ProjectileSpawnHandler projectileSpawnHandler in projectileSpawnHandlers)
        {
            totalSpawnWeight += projectileSpawnHandler.SpawnWeight;
        }
        float randomFloat = Random.Range(0f, totalSpawnWeight);
        foreach (ProjectileSpawnHandler projectileSpawnHandler in projectileSpawnHandlers)
        {
            randomFloat -= projectileSpawnHandler.SpawnWeight;
            if (randomFloat < 0)
            {
                return projectileSpawnHandler;
            }
        }
        return projectileSpawnHandlers[0];
    }

    private ProjectileSpawnHandler GetRandomGroupProjectileToSpawn()
    {
        return projectileSpawnHandlers[2];
    }

    private Projectile SpawnProjectile(ProjectileSpawnHandler projectileSpawnHandler)
    {
        Vector3 spawnPosition = Data.Projectile.Spawn.GetRandomSpawnPosition();
        Projectile projectile = ((GameObject)Instantiate(
            projectileSpawnHandler.GmObject,
            spawnPosition, Quaternion.identity)
            ).GetComponent<Projectile>();
        projectile.ID = ++currentProjectileID;
        projectile.MoveInDirection(Vector3.down);
        ActiveProjectiles.Add(projectile);
        return projectile;
    }

    public void EndGame()
    {
        foreach (Projectile projectile in ActiveProjectiles)
        {
            Destroy(projectile.gameObject);
        }
        ActiveProjectiles.Clear();
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnHandler
{
    public bool IsActive { get; set; }
    protected float enemySpawnWeight { get; set; }
    protected GameObject gmObject { get; set; }
    protected Enemy enemy { get; set; }
    public List<GameObject> EnemiesToSpawn { get; protected set; }
    protected int minGroupSize { get; set; }
    protected int maxGroupSize { get; set; }
    public Vector3 SpawnAnchor { get; protected set; }
    public bool Use { get; set; }

	
	public EnemySpawnHandler(GameObject gmObject, int minGroupSize, int maxGroupSize)
    {
        this.IsActive = false;
        this.gmObject = gmObject;
        this.enemy = gmObject.GetComponent<Enemy>();
        this.enemySpawnWeight = enemy.SpawnWeight;
        this.EnemiesToSpawn = new List<GameObject>();
        this.minGroupSize = minGroupSize;
        this.maxGroupSize = maxGroupSize;
        GetNextEnemiesToSpawn();
	}

    protected virtual void GetNextEnemiesToSpawn()
    {
        int nextNumberOfEnemies = GetNextNumberOfEnemiesToSpawn();
        EnemiesToSpawn.Clear();
        for (int i = 0; i < nextNumberOfEnemies; i++)
        {
            EnemiesToSpawn.Add(gmObject);
        }
    }

    protected int GetNextNumberOfEnemiesToSpawn()
    {
        if (enemy.GroupSizeWeights.Count <= 1)
        {
            return 1;
        }
        float randomFloat = Random.Range(0f, 1f);
        for (int i = 0; i < enemy.GroupSizeWeights.Count; i++)
        {
            randomFloat -= enemy.GroupSizeWeights[i];
            if (randomFloat < 0)
            {
                return i + 1;
            }
        }
        return 0;
    }

    public float GetTotalSpawnWeight()
    {

        return enemySpawnWeight / EnemiesToSpawn.Count;
    }

    public virtual void Activate()
    {
        IsActive = true;
        SpawnAnchor = Data.Enemy.Spawn.GetRandomSpawnPosition();
    }

    public virtual void Deactivate()
    {
        IsActive = false;
        GetNextEnemiesToSpawn();
    }
}

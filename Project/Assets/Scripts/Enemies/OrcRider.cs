﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OrcRider : Enemy
{
    public override float SpawnWeight
    {
        get { return Data.Enemy.OrcRider.SpawnWeight; }
    }
    public override List<float> GroupSizeWeights
    {
        get { return Data.Enemy.OrcRider.GroupSizeWeights; }
    }
    public override Data.Enemy.Type Type
    {
        get { return Data.Enemy.Type.OrcRider; }
    }
    public override float PhysicalWeight
    {
        get { return Data.Enemy.OrcRider.PhysicalWeight; }
    }
    public override float MaxHealth
    {
        get { return Data.Enemy.OrcRider.MaxHealth; }
    }
    public override float Damage
    {
        get { return Data.Enemy.OrcRider.Damage; }
    }
    public override float Points
    {
        get { return Data.Enemy.OrcRider.Points; }
    }
    public float FallDamageThreshold
    {
        get { return Data.Enemy.OrcRider.FallDamageThreshold; }
    } 
    protected override void Awake()
    {
        base.Awake();
        MoveSpeed = Data.Enemy.OrcRider.MoveSpeed;
    }

    public override void TakeDamage(Projectile projectile)
    {
        if (!projectileIDsDamagedBy.Contains(projectile.ID))
        {
            projectileIDsDamagedBy.Add(projectile.ID);
            CurrentHealth -= projectile.Damage;
            if (CurrentHealth <= 0)
            {
                EnemyHandler.Instance.PlayerKilledEnemy(this);
            }
            else
            {
                if (CurrentHealth <= MaxHealth * Data.Enemy.OrcRider.FallDamageThreshold)
                {
                    Fall();
                }
                else
                {
                    Knockback(projectile.Damage, projectile.transform.position);
                }
            }
        }
    }

    private void Fall()
    {
        Enemy orc = EnemyHandler.Instance.SpawnEnemy(Data.Enemy.Orc.GmObject, this.ID, this.Group, 
            this.transform.position, Vector3.zero);
        foreach (int id in projectileIDsDamagedBy)
        {
            orc.AddProjectileIDDamagedBy(id);
        }
        orc.SetHealth(CurrentHealth);
        orc.MoveInDirectionAfterTime(Vector3.down, 2f);
        EnemyHandler.Instance.PlayerKilledEnemy(this);
    }
}

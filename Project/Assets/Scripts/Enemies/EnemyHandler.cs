﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyHandler : Singleton<EnemyHandler> 
{
    protected EnemyHandler() { }

    public int EnemyID { get; protected set; }
    public int EnemyGroupID { get; protected set; }	
	private float nextSpawnSelectTime = 0f;
    private float nextSpawnTime = 0f;
	private List<EnemySpawnHandler> enemySpawnHandlers = new List<EnemySpawnHandler>();
    private EnemySpawnHandler selectedSpawn;
    private bool hasSelectedNextSpawn = false;
    public List<EnemyGroup> EnemyGroups = new List<EnemyGroup>();
    private EnemyGroup currentEnemyGroup;
    private const float maxEnemyPoints = 80;
    public float CurrentSpawnPoints = 0;
    public bool SpawnSkeletons = true;
    public bool SpawnOrcs = true;
    public bool SpawnOrcRiders = true;
    public bool SpawnOgres = true;
    public bool SpawnSiegeTowers = true;
	

	private void Start()
	{
		LoadEnemySpawnHandlers();
        EnemyID = 0;
        EnemyGroupID = 0;
        currentEnemyGroup = new EnemyGroup(++EnemyGroupID);
	}
	
	private void LoadEnemySpawnHandlers()
    {
        enemySpawnHandlers.Add(new EnemySpawnHandler(
             Data.Enemy.Skeleton.GmObject, Data.Enemy.Skeleton.MinGroupSize, Data.Enemy.Skeleton.MaxGroupSize));
        enemySpawnHandlers.Add(new EnemySpawnHandler(
             Data.Enemy.Orc.GmObject, Data.Enemy.Orc.MinGroupSize, Data.Enemy.Orc.MaxGroupSize));
        enemySpawnHandlers.Add(new EnemySpawnHandler(
             Data.Enemy.OrcRider.GmObject, Data.Enemy.OrcRider.MinGroupSize, Data.Enemy.OrcRider.MaxGroupSize));
        enemySpawnHandlers.Add(new EnemySpawnHandler(
             Data.Enemy.Ogre.GmObject, Data.Enemy.Ogre.MinGroupSize, Data.Enemy.Ogre.MaxGroupSize));
        enemySpawnHandlers.Add(new EnemySpawnHandler(
             Data.Enemy.SiegeTower.GmObject, Data.Enemy.SiegeTower.MinGroupSize, Data.Enemy.SiegeTower.MaxGroupSize));
        selectedSpawn = enemySpawnHandlers[0];
	}
	
	private void Update()
    {
        UpdatePlayerSpawnSelections();
        if (GameManager.Instance.State == GameManager.GameState.Play)
        {
            UpdateSpawns();
        }
	}

    private void UpdateSpawns()
    {
        if ((Time.time > nextSpawnSelectTime) && !selectedSpawn.IsActive)
        {
            if (!hasSelectedNextSpawn)
            {
                selectedSpawn = SelectNextSpawn();
                hasSelectedNextSpawn = true;
            }
            else if ((CurrentSpawnPoints + selectedSpawn.GetTotalSpawnWeight()) <= maxEnemyPoints)
            {
                selectedSpawn.Activate();
                nextSpawnSelectTime = Time.time + Data.Enemy.Spawn.GetWaitTime();
                hasSelectedNextSpawn = false;
            }
        }
        if (selectedSpawn.IsActive && Time.time > nextSpawnTime)
        {
            if (selectedSpawn.EnemiesToSpawn.Count > 0)
            {
                Vector3 position = Data.Enemy.Spawn.GetRandomSpawnPositionFromGroupAnchor(selectedSpawn.SpawnAnchor);
                SpawnEnemy(selectedSpawn.EnemiesToSpawn[0], position);
                selectedSpawn.EnemiesToSpawn.RemoveAt(0);
                nextSpawnTime = Time.time + Data.Enemy.Spawn.GetRandomGroupWaitTime();
            }
            else
            {
                EnemyGroups.Add(currentEnemyGroup);
                currentEnemyGroup = new EnemyGroup(++EnemyGroupID);
                selectedSpawn.Deactivate();
            }
        }
    }

    private void UpdatePlayerSpawnSelections()
    {
        bool[] selectedEnemySpawns = 
        {
            SpawnSkeletons, SpawnOrcs, SpawnOrcRiders, SpawnOgres, SpawnSiegeTowers
        };
        for (int i = 0; i < selectedEnemySpawns.Length; i++)
        {
            enemySpawnHandlers[i].Use = selectedEnemySpawns[i];
        }
    }

    private EnemySpawnHandler SelectNextSpawn()
    {
        float totalSpawnWeight = 0f;
        foreach (EnemySpawnHandler enemySpawnhandler in enemySpawnHandlers)
        {
            if (enemySpawnhandler.Use)
            {
                totalSpawnWeight += enemySpawnhandler.GetTotalSpawnWeight();
            }
        }
        float randomFloat = Random.Range(0f, totalSpawnWeight);
        foreach (EnemySpawnHandler enemySpawnhandler in enemySpawnHandlers)
        {
            if (enemySpawnhandler.Use)
            {
                randomFloat -= enemySpawnhandler.GetTotalSpawnWeight();
                if (randomFloat < 0f)
                {
                    return enemySpawnhandler;
                }
            }
        }
        return null;
    }

    public Enemy SpawnEnemy(GameObject gmObject, Vector3 position)
    {
        return SpawnEnemy(gmObject, ++EnemyID, currentEnemyGroup, position, Vector3.down);
    }

    public Enemy SpawnEnemy(GameObject gmObject, Vector3 position, Vector3 moveDirection)
    {
        return SpawnEnemy(gmObject, ++EnemyID, currentEnemyGroup, position, moveDirection);
    }

    public Enemy SpawnEnemy(GameObject gmObject, int id, EnemyGroup enemyGroup, Vector3 position, Vector3 moveDirection)
    {
        Enemy enemy = ((GameObject)Instantiate(gmObject, position, Quaternion.Euler(new Vector3(0, 0, 180)))).GetComponent<Enemy>();
        enemy.ID = id;
        enemy.Group = enemyGroup;
        enemyGroup.Enemies.Add(enemy);
        enemy.transform.position = new Vector3(enemy.transform.position.x, enemy.transform.position.y, Data.PositionLayer.Enemy);
        enemy.MoveInDirection(moveDirection);
        CurrentSpawnPoints += enemy.Points;
        return enemy;
    }
	
	public void PlayerKilledEnemy(Enemy enemy)
	{
		ScoreHandler.Instance.PlayerKilledEnemy(enemy);
		DespawnEnemy(enemy);
	}
	
	public void EnemyReachedBase(Enemy enemy)
	{
		BaseHandler.Instance.TakeDamage(enemy.Damage);
		DespawnEnemy(enemy);
	}
	
	public void DespawnEnemy(Enemy enemy)
	{
        //print("Removing " + enemy.name + "[" + enemy.Group.ID + "] from Group " + enemy.Group.ID);
        EnemyGroup enemyGroup = enemy.Group;
        RemoveEnemyFromItsGroup(enemy);
        CheckGroupIsStillActive(enemyGroup);
		CurrentSpawnPoints -= enemy.Points;        
		Destroy(enemy.gameObject);
        //print(group.Enemies.Count);
	}

    private void RemoveEnemyFromItsGroup(Enemy enemy)
    {
        for (int i = EnemyGroups.Count - 1; i >= 0; i--)
        {
            if (EnemyGroups[i].ID == enemy.Group.ID)
            {
                EnemyGroups[i].RemoveEnemy(enemy);
                break;
            }
        }
    }

    private void CheckGroupIsStillActive(EnemyGroup enemyGroup)
    {
        if (!enemyGroup.HasEnemies())
        {
            EnemyGroups.Remove(enemyGroup);
        }
    }

    public void EndGame()
    {
        for (int i = EnemyGroups.Count - 1; i >= 0; i--)
        {
            for (int j = EnemyGroups[i].Enemies.Count - 1; j >= 0; j--)
            {
                DespawnEnemy(EnemyGroups[i].Enemies[j]);
            }
        }
    }
}

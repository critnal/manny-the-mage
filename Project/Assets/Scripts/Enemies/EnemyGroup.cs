﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyGroup {

    public int ID { get; set; }
    public List<Enemy> Enemies { get; set; }
    protected List<Enemy> potentialEnemies { get; set; }


    public EnemyGroup(int id)
    {
        this.ID = id;
        Enemies = new List<Enemy>();
        potentialEnemies = new List<Enemy>();
    }

    public bool HasEnemies()
    {
        return Enemies.Count > 0;
    }

    public bool HasPotentialEnemies()
    {
        return GetPotentialEnemies().Count > 0;
    }

    public List<Enemy> GetPotentialEnemies()
    {
        potentialEnemies.Clear();
        foreach (Enemy enemy in Enemies)
        {
            if (enemy.CurrentPotentialHealth > 0)
            {
                potentialEnemies.Add(enemy);
            }
        }
        return potentialEnemies;
    }

    public void RemoveEnemy(Enemy enemy)
    {
        Enemies.Remove(enemy);
    }
}

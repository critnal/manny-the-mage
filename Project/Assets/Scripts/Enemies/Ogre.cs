using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ogre : Enemy
{
    public override float SpawnWeight
    {
        get { return Data.Enemy.Ogre.SpawnWeight; }
    }
    public override List<float> GroupSizeWeights
    {
        get { return Data.Enemy.Ogre.GroupSizeWeights; }
    }
    public override Data.Enemy.Type Type
    {
        get { return Data.Enemy.Type.Ogre; }
    }
    public override float PhysicalWeight
    {
        get { return Data.Enemy.Ogre.PhysicalWeight; }
    }
    public override float MaxHealth
    {
        get { return Data.Enemy.Ogre.MaxHealth; }
    }
    public override float Damage
    {
        get { return Data.Enemy.Ogre.Damage; }
    }
    public override float Points
    {
        get { return Data.Enemy.Ogre.Points; }
    }


    protected override void Awake()
    {
        base.Awake();
        MoveSpeed = Data.Enemy.Ogre.MoveSpeed;
    }

    public override void TakeDamage(Projectile projectile)
    {
        if (!projectileIDsDamagedBy.Contains(projectile.ID))
        {
            projectileIDsDamagedBy.Add(projectile.ID);
            CurrentHealth -= projectile.Damage;
            if (CurrentHealth <= 0)
            {
                EnemyHandler.Instance.PlayerKilledEnemy(this);
            }
            else
            {
                Knockback(projectile.Damage, projectile.transform.position);
                MoveSpeed = Data.Enemy.Ogre.MoveSpeed + (1 - (CurrentHealth / MaxHealth)) * Data.Enemy.Ogre.ExtraMoveSpeed * Data.Instance.StandardMoveSpeed;
            }
        }
    }
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Enemy : MonoBehaviour
{
    public int ID { get; set; }
    public EnemyGroup Group { get; set; }
    public abstract float SpawnWeight { get; }
    public abstract List<float> GroupSizeWeights { get; }
    public abstract Data.Enemy.Type Type { get; }
    public abstract float PhysicalWeight { get; }
    public abstract float MaxHealth { get; }
    public abstract float Damage { get; }
    public abstract float Points { get; }
    public float MoveSpeed { get; set; }
    public float CurrentHealth { get; protected set; }
    public float CurrentPotentialHealth { get; protected set; }
    public float KnockbackEffectEndTime { get; protected set; }
    protected List<int> projectileIDsDamagedBy { get; set; }
	public Vector3 KnockbackVelocity { get;	protected set; }
	protected bool hasResetKnockback = true;

	
	protected Enemy() {}

    protected virtual void Awake()
    {
        //this.transform.rotation = Camera.main.transform.rotation;
        CurrentHealth = MaxHealth;
        CurrentPotentialHealth = MaxHealth;
        projectileIDsDamagedBy = new List<int>();
    }
	
	public void MoveInDirection(Vector3 direction)
	{
		this.rigidbody.velocity = direction.normalized * MoveSpeed;		
	}

    public void MoveInDirectionAfterTime(Vector3 direction, float time)
    {
        StartCoroutine(MoveInDirection(direction, time));
    }

    protected IEnumerator MoveInDirection(Vector3 direction, float time)
    {
        yield return new WaitForSeconds(time);
        this.rigidbody.velocity = direction.normalized * MoveSpeed;
    }
	
	protected virtual void OnTriggerEnter(Collider other)
	{
		if (other.tag == Data.Tag.DespawnZone)
		{
			EnemyHandler.Instance.DespawnEnemy(this);
		}
		else if (other.tag == Data.Tag.Base)
		{
			EnemyHandler.Instance.EnemyReachedBase(this);
		}
	}

    public virtual void SetHealth(float health)
    {
        CurrentHealth = health;
        if (CurrentHealth > MaxHealth)
        {
            CurrentHealth = MaxHealth;
        }
    }

    public virtual void TakeDamage(Projectile projectile)
    {
        if (!projectileIDsDamagedBy.Contains(projectile.ID))
        {
            projectileIDsDamagedBy.Add(projectile.ID);
            CurrentHealth -= projectile.Damage;
            if (CurrentHealth <= 0)
            {
                EnemyHandler.Instance.PlayerKilledEnemy(this);
            }
            else
            {
                Knockback(projectile.Damage, projectile.transform.position);
            }
        }
    }

    public virtual void TakePotentialDamage(float damage)
    {
        CurrentPotentialHealth -= damage;
    }
	
	public void Knockback(float damage, Vector3 position)
	{
		KnockbackEffectEndTime = Time.time + Data.Enemy.Knockback.Duration;
		Vector3 direction = Vector3.Normalize(this.transform.position - position);
		KnockbackVelocity = direction * damage / PhysicalWeight;
		hasResetKnockback = false;
		this.rigidbody.velocity = KnockbackVelocity;
	}
	
	protected void FixedUpdate()
	{
		if (KnockbackEffectEndTime > Time.time)
		{
			this.rigidbody.velocity = new Vector3(
				Mathf.Lerp(this.rigidbody.velocity.x, 0f, Data.Enemy.Knockback.Duration),
				Mathf.Lerp(this.rigidbody.velocity.y, 0f, Data.Enemy.Knockback.Duration),
				0f);
			
		}
		else if (!hasResetKnockback)
		{
			MoveInDirection(Vector3.down);
			hasResetKnockback = true;
		}
	}

    public void AddProjectileIDDamagedBy(int id)
    {
        projectileIDsDamagedBy.Add(id);
    }
}

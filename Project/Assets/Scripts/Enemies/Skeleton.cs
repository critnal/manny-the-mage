using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skeleton : Enemy
{
    public override float SpawnWeight
    {
        get { return Data.Enemy.Skeleton.SpawnWeight; }
    }
    public override List<float> GroupSizeWeights
    {
        get { return Data.Enemy.Skeleton.GroupSizeWeights; }
    }
    public override Data.Enemy.Type Type
    {
        get { return Data.Enemy.Type.Skeleton; }
    }
    public override float PhysicalWeight
    {
        get { return Data.Enemy.Skeleton.PhysicalWeight; }
    }
    public override float MaxHealth
    {
        get { return Data.Enemy.Skeleton.MaxHealth; }
    }
    public override float Damage
    {
        get { return Data.Enemy.Skeleton.Damage; }
    }
    public override float Points
    {
        get { return Data.Enemy.Skeleton.Points; }
    }
    protected override void Awake()
    {
        base.Awake();
        MoveSpeed = Data.Enemy.Skeleton.MoveSpeed;
    }
}

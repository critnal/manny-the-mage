using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Orc : Enemy
{
    public override float SpawnWeight
    {
        get { return Data.Enemy.Orc.SpawnWeight; }
    }
    public override List<float> GroupSizeWeights
    {
        get { return Data.Enemy.Orc.GroupSizeWeights; }
    }
    public override Data.Enemy.Type Type
    {
        get { return Data.Enemy.Type.Orc; }
    }
    public override float PhysicalWeight
    {
        get { return Data.Enemy.Orc.PhysicalWeight; }
    }
    public override float MaxHealth
    {
        get { return Data.Enemy.Orc.MaxHealth; }
    }
    public override float Damage
    {
        get { return Data.Enemy.Orc.Damage; }
    }
    public override float Points
    {
        get { return Data.Enemy.Orc.Points; }
    }


	protected override void Awake()
	{
        base.Awake();
		MoveSpeed = Data.Enemy.Orc.MoveSpeed;
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SiegeTower : Enemy
{
    public override float SpawnWeight
    {
        get { return Data.Enemy.SiegeTower.SpawnWeight; }
    }
    public override List<float> GroupSizeWeights
    {
        get { return Data.Enemy.SiegeTower.GroupSizeWeights; }
    }
    public override Data.Enemy.Type Type
    {
        get { return Data.Enemy.Type.SiegeTower; }
    }
    public override float PhysicalWeight
    {
        get { return Data.Enemy.SiegeTower.PhysicalWeight; }
    }
    public override float MaxHealth
    {
        get { return Data.Enemy.SiegeTower.MaxHealth; }
    }
    public override float Damage
    {
        get { return Data.Enemy.SiegeTower.Damage; }
    }
    public override float Points
    {
        get { return Data.Enemy.SiegeTower.Points; }
    }
    protected List<LoadableEnemy> loadableEnemies { get; set; }


    public class LoadableEnemy
    {
        public GameObject GmObject { get; protected set; }
        public Enemy Enemy { get; protected set; }


        public LoadableEnemy(GameObject gmObject)
        {
            this.GmObject = gmObject;
            this.Enemy = gmObject.GetComponent<Enemy>();
        }
    }


    protected override void Awake()
    {
        base.Awake();
        MoveSpeed = Data.Enemy.SiegeTower.MoveSpeed;
        loadableEnemies = new List<LoadableEnemy>() 
        { 
            new LoadableEnemy(Data.Enemy.Skeleton.GmObject), 
            new LoadableEnemy(Data.Enemy.Orc.GmObject), 
            new LoadableEnemy(Data.Enemy.OrcRider.GmObject), 
            new LoadableEnemy(Data.Enemy.Ogre.GmObject)
        };
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.tag == Data.Tag.DespawnZone)
        {
            EnemyHandler.Instance.DespawnEnemy(this);
        }
        else if (other.tag == Data.Tag.SiegeTowerUnloadTrigger)
        {
            SpawnLoadedEnemies();
            EnemyHandler.Instance.DespawnEnemy(this);
        }
    }

    public override void TakeDamage(Projectile projectile)
    {
        if (!projectileIDsDamagedBy.Contains(projectile.ID))
        {
            projectileIDsDamagedBy.Add(projectile.ID);
            CurrentHealth -= projectile.Damage;
            if (CurrentHealth <= 0)
            {
                SpawnLoadedEnemies();
                EnemyHandler.Instance.PlayerKilledEnemy(this);
            }
            else
            {
                Knockback(projectile.Damage, projectile.transform.position);
            }
        }
    }

    private void SpawnLoadedEnemies()
    {
        foreach (LoadableEnemy loadableEnemy in GetLoadedEnemies())
        {
            Enemy enemy = EnemyHandler.Instance.SpawnEnemy(loadableEnemy.GmObject, this.ID, this.Group, 
                Data.Enemy.Spawn.GetRandomSpawnPositionFromGroupAnchor(this.transform.position), Vector3.down);
            foreach (int id in projectileIDsDamagedBy)
            {
                enemy.AddProjectileIDDamagedBy(id);
            }
        }
    }

    private List<LoadableEnemy> GetLoadedEnemies()
    {
        List<LoadableEnemy> loadedEnemies = new List<LoadableEnemy>();
        float loadedPoints = 0f;
        while (loadableEnemies.Count > 0)
        {
            LoadableEnemy enemyToLoad = loadableEnemies[Random.Range(0, loadableEnemies.Count)];
            loadedEnemies.Add(enemyToLoad);
            loadedPoints += enemyToLoad.Enemy.Points;
            for (int i = loadableEnemies.Count - 1; i >= 0; i--)
            {
                if (loadableEnemies[i].Enemy.Points > (Data.Enemy.SiegeTower.MaxLoadedPoints - loadedPoints))
                {
                    loadableEnemies.RemoveAt(i);
                }
            }
        }
        return loadedEnemies;
    }
}

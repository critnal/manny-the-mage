﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class Data : Singleton<Data>
{
	protected Data() {}
	
	public float StandardMoveSpeed = 1f;
	public float Skeleton = 1.5f;
	public float Orc = 1.0f;
	public float OrcRider = 2.0f;
	public float Ogre = 0.5f;
	public float SiegeTower = 0.3f;
	public float Arrow = 4.0f;
	public float Boulder = 2.0f;
	public float Fireball = 2.5f;
	
	private void LateUpdate()
	{
		Enemy.Skeleton.MoveSpeed = Skeleton * StandardMoveSpeed;
		Enemy.Orc.MoveSpeed = Orc * StandardMoveSpeed;
		Enemy.OrcRider.MoveSpeed = OrcRider * StandardMoveSpeed;
		Enemy.Ogre.MoveSpeed = Ogre * StandardMoveSpeed;
		Enemy.SiegeTower.MoveSpeed = SiegeTower * StandardMoveSpeed;
		Projectile.Arrow.MoveSpeed = Arrow * StandardMoveSpeed;
		Projectile.Boulder.MoveSpeed = Boulder * StandardMoveSpeed;
		Projectile.Fireball.MoveSpeed = Fireball * StandardMoveSpeed;
	}
	
	void Awake()
	{
		Data.Enemy.Spawn.LeftSpawnBoundary = GameObject.Find("LeftSpawnBoundary");
		Data.Enemy.Spawn.RightSpawnBoundary =  GameObject.Find("RightSpawnBoundary");
        Data.Enemy.Spawn.SpawnZoneLength 
            = Vector3.Distance(Data.Enemy.Spawn.LeftSpawnBoundary.transform.position, 
            Data.Enemy.Spawn.RightSpawnBoundary.transform.position);
        Data.Projectile.Spawn.LeftSpawnBoundary = Data.Enemy.Spawn.LeftSpawnBoundary;
        Data.Projectile.Spawn.RightSpawnBoundary = Data.Enemy.Spawn.RightSpawnBoundary;
        Data.Projectile.Spawn.SpawnZoneLength = Data.Enemy.Spawn.SpawnZoneLength;
	}
	
	public static class PositionLayer
	{
		public const float Projectile = 0f;
		public const float Enemy = 2f;
		
	}
	
	public static class Tag	
	{
		public const string Enemy = "Enemy";
		public const string Projectile = "Projectile";
        public const string DespawnZone = "DespawnZone";
        public const string Base = "Base";
        public const string SiegeTowerUnloadTrigger = "SiegeTowerUnloadTrigger";
	}
	
	public static class Input
	{	
		public enum AimLineState
		{
			None = 0,
			NoProjectileSelected,
			ProjectileSelected
		}
		
		public const float AimDistanceSpeedModifier = 2f;
		public const float StaticReleaseSpeed = 20f;
	}
	
	public static class Score
	{
		public const float ComboPointDecrementInterval = 1.0f;
		public const float PlusScoreLabelScrollSpeed = 0.5f;
		public const float PlusScoreLabelDuration = 2;
        public const float MaxComboPoints = 5f;
        public const float ComboDuration = 2.0f;
	}
	
	public static class Base
	{
		public const float MaxHealth = 100f;
	}
		
	public static class Enemy
	{
		public static class Skeleton
		{
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Skeleton");
			public static float MoveSpeed = 1.5f;
            public const float MaxHealth = 10;
            public const float Damage = 5;
            public const float Points = 4;
            public const float SpawnWeight = 10;
            public const float PhysicalWeight = 1;
            public const int MinGroupSize = 2;
            public const int MaxGroupSize = 6;
            public static List<float> GroupSizeWeights = new List<float>
            {
                0f, 0f, 0.3f, 0.4f, 0.2f, 0.1f
            };
		}
		public static class Orc
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Orc");
			public static float MoveSpeed = 1;
			public const float MaxHealth = 10;
            public const float Damage = 10;
            public const float Points = 6;
            public const float SpawnWeight = 10;
            public const float PhysicalWeight = 1;
            public const int MinGroupSize = 1;
            public const int MaxGroupSize = 1;
            public static List<float> GroupSizeWeights = new List<float>
            {
               0.5f, 0.25f, 0.15f, 0.1f
            };
		}		
		public static class OrcRider
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/OrcRider");
			public static float MoveSpeed = 2;
			public const float MaxHealth = 20;
            public const float Damage = 15;
            public const float Points = 8;
            public const float SpawnWeight = 8;
            public const float PhysicalWeight = 1.5f;
            public const int MinGroupSize = 1;
            public const int MaxGroupSize = 3;
            public static List<float> GroupSizeWeights = new List<float>
            {
                0.7f, 0.2f, 0.1f
            };
            public const float FallDamageThreshold = 0.9f;
		}
		public static class Ogre
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Ogre");
			public static float MoveSpeed = 0.5f;
            public const float ExtraMoveSpeed = 2f;
			public const float MaxHealth = 45;
            public const float Damage = 30;
            public const float Points = 12;
            public const float SpawnWeight = 4;
            public const float PhysicalWeight = 3f;
            public const int MinGroupSize = 1;
            public const int MaxGroupSize = 3;
            public static List<float> GroupSizeWeights = new List<float>
            {
                 0.7f, 0.2f, 0.1f
            };
		}	
		public static class SiegeTower
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/SiegeTower");
			public static float MoveSpeed = 0.3f;
			public const float MaxHealth = 100;
            public const float Damage = 1;
            public const float Points = 20;
            public const float MaxLoadedPoints = 24f;
            public const float SpawnWeight = 1;
            public const float PhysicalWeight = 10f;
            public const int MinGroupSize = 1;
            public const int MaxGroupSize = 2;
            public static List<float> GroupSizeWeights = new List<float>
            {
                0.9f, 0.1f
            };
		}	
		public enum Type
		{
			None,
			Skeleton,
			Orc,
			OrcRider,
			Ogre,
			SiegeTower
		}
		public static class Spawn
		{
			public const float MinWaitTime = 0.5f;
			public const float MaxWaitTime = 2.0f;
			public const float MinGroupWaitTime = 0.2f;
			public const float MaxGroupWaitTime = 0.4f;
			public static GameObject LeftSpawnBoundary;
			public static GameObject RightSpawnBoundary;
			public static float SpawnZoneLength;
            public const float MaxGroupSpread = 1f;
			
			public static float GetWaitTime()
			{
                //return Random.Range(MinWaitTime, MaxWaitTime);
                return 3f;
			}
			
			public static float GetRandomGroupWaitTime()
			{
				return Random.Range(MinGroupWaitTime, MaxGroupWaitTime);
			}

            public static Vector3 GetRandomSpawnPosition()
            {
                return new Vector3(
                    LeftSpawnBoundary.transform.position.x + Random.Range(0f, SpawnZoneLength),
                    LeftSpawnBoundary.transform.position.y,
                    0f);
            }

            public static Vector3 GetRandomSpawnPositionFromGroupAnchor(Vector3 anchor)
            {
                return new Vector3(
                    anchor.x + Random.Range(-MaxGroupSpread, MaxGroupSpread),
                    anchor.y,
                    0f);
            }
		}
		public static class Knockback
		{
			public const float Duration = 0.5f;
		}
	}
	
	public static class Projectile
	{		
		public static class Arrow
		{
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Arrow");
            public static float MoveSpeed = 4f;
            public const float Damage = 10f;
            public const float SpawnWeight = 10f;
		}
		public static class Boulder
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Boulder");
			public static float MoveSpeed = 2f;
            public const float Damage = 35f;
            public const float SpawnWeight = 1f;
		}
		public static class Fireball
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Fireball");
			public static float MoveSpeed = 2.5f;
            public const float Damage = 25f;
            public const float SpawnWeight = 2f;
		}
		public static class Explosion
        {
            public static readonly GameObject GmObject = (GameObject)Resources.Load("Prefabs/Explosion");
			public const float Damage = 25f;
			public const float TriggerDuration = 0.1f;
			public const float VisualDuration = 0.4f;
		}

        public static class Spawn
        {
            public const float MinWaitTime = 0.5f;
            public const float MaxWaitTime = 2.0f;
            public static GameObject LeftSpawnBoundary;
            public static GameObject RightSpawnBoundary;
            public static float SpawnZoneLength;

            public static float GetWaitTime()
            {
                //return Random.Range(MinWaitTime, MaxWaitTime);
                return 2f;
            }

            public static Vector3 GetRandomSpawnPosition()
            {
                return new Vector3(
                    LeftSpawnBoundary.transform.position.x + Random.Range(0f, SpawnZoneLength),
                    LeftSpawnBoundary.transform.position.y,
                    Data.PositionLayer.Projectile);
            }
        }
	}
}

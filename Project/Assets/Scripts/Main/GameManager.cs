﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class GameManager : Singleton<GameManager>
{
    private GameManager() { }

    public enum GameState
    {
        Menu, Play
    }
    public GameState State { get; private set; }


    private void Awake()
    {
        State = GameState.Menu;
    }
	
	private void Update()
	{
		
	}

    public void StartGame()
    {
        State = GameState.Play;
        ScoreHandler.Instance.StartGame();
        BaseHandler.Instance.StartGame();
    }

    public void EndGame()
    {
        State = GameState.Menu;
        EnemyHandler.Instance.EndGame();
        ProjectileHandler.Instance.EndGame();
    }
}

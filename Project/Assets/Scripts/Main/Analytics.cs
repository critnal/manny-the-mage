﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Analytics : Singleton<Analytics> 
{
	public class EnemyData
	{
		public Data.Enemy.Type Type
		{
			get;
			set;
		}
		public float SpawnCount
		{
			get;
			set;
		}		
		public EnemyData(Data.Enemy.Type enemyType)
		{
			this.Type = enemyType;
			this.SpawnCount = 0;
			SpawnPriorityFractions = new List<float>();
		}		
		public List<float> SpawnPriorityFractions
		{
			get;
			set;
		}
	}
	
	protected Analytics() 
	{
	}
	
	private List<EnemyData> enemiesData = new List<EnemyData>();
	private float currentConsecutiveSpawnCount = 0;
	private List<float> consecutiveSpawnCounts = new List<float>();
	private Data.Enemy.Type lastEnemyTypeSpawned = Data.Enemy.Type.None;
	private float totalSpawnCount = 0;
	
	public void AddEnemyData(Data.Enemy.Type enemyType)
	{
		print("Adding " + enemyType.ToString());
		enemiesData.Add(new EnemyData(enemyType));
	}
	
	public void IncrementEnemyTypeSpawnCount(Data.Enemy.Type enemyType)
	{
		totalSpawnCount++;
//		print("Enemy type: " + enemyType.ToString() + " ===============================================================");
		foreach (EnemyData data in enemiesData)
		{
//			print("Current enemy type: " + data.Type.ToString());
			if (data.Type == enemyType)
			{
				data.SpawnCount++;
//				print("Types match");
//				print("Last spawned type: " + lastEnemyTypeSpawned.ToString());
				if (lastEnemyTypeSpawned == enemyType)
				{
//					print("Consecutive spawn");
					currentConsecutiveSpawnCount++;
//					print("Current consecutive spawn count: " + currentConsecutiveSpawnCount);
				}
				else
				{
//					print("Not consecutive");
					lastEnemyTypeSpawned = data.Type;
					consecutiveSpawnCounts.Add(currentConsecutiveSpawnCount);
//					print("Consecutive streak added: " + currentConsecutiveSpawnCount);
					currentConsecutiveSpawnCount = 0;
//					print("Current consecutive spawn count: " + currentConsecutiveSpawnCount);
				}
			}
//			else
//			{
//				print("Types do not match");	
//			}
		}
	}	
	
	public void OutputEnemySpawnRatios()
	{
		System.Text.StringBuilder builder = new System.Text.StringBuilder();
		builder.Append("Spawn Frequencies: ");
//		float spawnSum = 0;
//		foreach (EnemyData enemType in enemiesData)
//		{
//			spawnSum += enemType.SpawnCount;
//		}
		foreach (EnemyData enemyType in enemiesData)
		{
			float ratio = enemyType.SpawnCount / totalSpawnCount;
			builder.Append("[").Append(enemyType.Type.ToString()).Append("] ").Append((ratio * 100).ToString("n2")).Append("%, ").Append(enemyType.SpawnCount).Append("       ");
		}
		print(builder);
	}
	
	public void OutputEnemyConsecutiveSpawnDetails()
	{
		System.Text.StringBuilder builder = new System.Text.StringBuilder();
//		float spawnSum = 1;
		float totalConsecutiveSpawns = 0;
//		float totalConsecutiveSpawnStreaks = 0;
//		spawnSum += enemType.SpawnCount;
		foreach (int spawnStreak in consecutiveSpawnCounts)
		{
			totalConsecutiveSpawns += spawnStreak;
//				totalConsecutiveSpawnStreaks++;
		}
		builder.Append("Consecutive Spawns / Total Spawns = ").Append(totalConsecutiveSpawns).Append(" " +
			"/ ").Append(totalSpawnCount).Append(" = ").Append(totalConsecutiveSpawns / totalSpawnCount);
		print(builder);
		builder = new System.Text.StringBuilder();
		float consecutiveStreakAverage = totalConsecutiveSpawns / consecutiveSpawnCounts.Count;
		builder.Append("Consecutive Spawns / Number of Consecutive Streaks (Average Streak Size) = ").Append(totalConsecutiveSpawns).Append(
			" / ").Append(consecutiveSpawnCounts.Count).Append(" = ").Append(consecutiveStreakAverage);
		print(builder);
		float streakAverageDivergenceTotal = 0f;
		foreach (int spawnStreak in consecutiveSpawnCounts)
		{
			streakAverageDivergenceTotal += Mathf.Abs(spawnStreak - consecutiveStreakAverage);
		}
		float streakAverageDivergenceAverage = streakAverageDivergenceTotal / consecutiveSpawnCounts.Count;
		builder = new System.Text.StringBuilder();
		builder.Append("Streak Average Divergence Average = ").Append(streakAverageDivergenceAverage);
		print(builder);
	}
	
	public void AddEnemyTypeFraction(Data.Enemy.Type enemyType, float fraction)
	{
		foreach (EnemyData data in enemiesData)
		{
			if (data.Type == enemyType)
			{
				data.SpawnPriorityFractions.Add(fraction);
				return;
			}
		}
	}
	
	public void OutputEnemyFractionAverages()
	{
		System.Text.StringBuilder builder = new System.Text.StringBuilder();
		builder.Append("Fraction Averages: ");
		foreach (EnemyData enemyType in enemiesData)
		{
			float fractionTotal = 0f;
			foreach (float fraction in enemyType.SpawnPriorityFractions)
			{
				fractionTotal += fraction;
			}
			float average = fractionTotal / enemyType.SpawnPriorityFractions.Count;
			builder.Append(average).Append("[").Append(enemyType.Type.ToString()).Append("] ");
		}
		print(builder);
	}
}

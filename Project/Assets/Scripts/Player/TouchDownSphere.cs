﻿using UnityEngine;
using System.Collections;

public class TouchDownSphere : Singleton<TouchDownSphere> 
{
	protected TouchDownSphere() {}
	
	public bool Enabled
	{
		get;
		private set;
	}
	public bool HasSelectedAProjectile
	{
		get;
		private set;
	}
	public GameObject SelectedProjectile
	{
		get;
		private set;
	}

	private void Start() 
	{
		Enabled = false;
		HasSelectedAProjectile = false;
		MoveToStandyByPosition();
	}
	
	private void OnTriggerStay(Collider other)
	{
		if (Enabled && InputHandler.Instance.InputSystem == 1)
		{
			if (other.tag == Data.Tag.Projectile)
			{
				SelectedProjectile = other.gameObject;
				HasSelectedAProjectile = true;
				Enabled = false;
				MoveToStandyByPosition();
			}
		}
	}
	
	public void Set(Vector3 position)
	{
		this.transform.position = position;
		Enabled = true;
		HasSelectedAProjectile = false;
	}
	
	public void MoveToStandyByPosition()
	{		
		this.transform.position = new Vector3(-10f, 0f, 0f);
	}
}

﻿using UnityEngine;
using System.Collections;

public class BaseHandler : Singleton<BaseHandler> 
{
	protected BaseHandler() {}
	
	public float MaxHealth { get; private set; }
	public float Health { get; private set; }
	
	private void Awake()
	{
		MaxHealth = Data.Base.MaxHealth;
		Health = MaxHealth;
	}
	
	public void TakeDamage(float damage)
	{
		Health -= damage;
		if (Health <= 0)
		{
            GameManager.Instance.EndGame();
		}
	}

    public void StartGame()
    {
        Health = MaxHealth;
    }
}

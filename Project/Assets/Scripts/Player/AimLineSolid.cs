﻿using UnityEngine;
using System.Collections;

public class AimLineSolid : Singleton<AimLineSolid> 
{
	protected AimLineSolid() {}
	
	private LineRenderer lineRenderer;
	
	private void Awake()
	{
		lineRenderer = this.GetComponent<LineRenderer>();
	}	
	
	private void Update()
	{
		if (InputHandler.Instance.AimLineState == Data.Input.AimLineState.None)
		{
			lineRenderer.enabled = false;
		}
		else if (InputHandler.Instance.AimLineState == Data.Input.AimLineState.NoProjectileSelected)
		{
			lineRenderer.enabled = true;
			lineRenderer.SetPosition(0, InputHandler.Instance.LastMouseDownPosition);
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			lineRenderer.SetPosition(1, new Vector3(mousePosition.x, mousePosition.y, InputHandler.Instance.LastMouseDownPosition.z));
		}
		else if (InputHandler.Instance.AimLineState == Data.Input.AimLineState.ProjectileSelected)
		{
            if (InputHandler.Instance.SelectedProjectile != null)
            {
                if (InputHandler.Instance.InputSystem == 2)
                {
                    lineRenderer.enabled = true;
                    lineRenderer.SetPosition(0, InputHandler.Instance.SelectedProjectile.transform.position);
                    lineRenderer.SetPosition(1, new Vector3(InputHandler.Instance.MousePosition.x, InputHandler.Instance.MousePosition.y,
                        InputHandler.Instance.SelectedProjectile.transform.position.z));
                }
                else if (InputHandler.Instance.SelectedEnemy  != null)
                {
                    lineRenderer.enabled = true;
                    lineRenderer.SetPosition(0, InputHandler.Instance.SelectedProjectile.transform.position);
                    Vector3 enemyPosition = InputHandler.Instance.SelectedEnemy.transform.position;
                    lineRenderer.SetPosition(1, new Vector3(enemyPosition.x, enemyPosition.y, InputHandler.Instance.SelectedProjectile.transform.position.z));
                }
            }
		}
	}
}

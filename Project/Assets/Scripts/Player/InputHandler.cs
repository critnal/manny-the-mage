﻿using UnityEngine;
using System.Collections;

public class InputHandler : Singleton<InputHandler>
{
	protected InputHandler() {}

    public GameObject SelectedProjectile { get; private set; }
    public Data.Input.AimLineState AimLineState { get; private set; }
    public Vector3 MousePosition { get; private set; }
    public Vector3 LastMouseDownPosition { get; private set; }
    public GameObject SelectedEnemy { get; private set; }
	
	public int InputSystem = 3;
	private bool inputDown = false;
	private bool inputUp = false;
	private bool hasLastMouseDownPosition = false;
	private LayerMask projectileLayer;
	
	private void Update()
	{		
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePosition.z = 0f;
		MousePosition = mousePosition;
		
		inputUp = Input.GetMouseButtonUp(0);		
		if (!inputDown)
		{			
			inputDown = Input.GetMouseButtonDown(0);
		} 
		else if (inputUp)
		{
			inputDown = false;
		}
		HandleInputDown();
        HandleInputUp();
        if (InputSystem == 3 && AimLineState == Data.Input.AimLineState.ProjectileSelected && EnemyHandler.Instance.EnemyGroups.Count > 0)
        {
            SelectedEnemy = FindClosestEnemy();
        }
        UpdateInputSystem();
	}
	
	private void HandleInputDown()
	{
		if (inputDown)
		{
			if (InputSystem == 0)
			{
				if (!hasLastMouseDownPosition)
				{
					hasLastMouseDownPosition = true;
					LastMouseDownPosition = MousePosition;
					AimLineState = Data.Input.AimLineState.NoProjectileSelected;
				}
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(
					LastMouseDownPosition,
					MousePosition - LastMouseDownPosition, 
					out hit, 
					Vector3.Distance(LastMouseDownPosition, MousePosition), 
					1 << LayerMask.NameToLayer("Projectile"))
					||
					Physics.Raycast(ray, out hit, 20f, 1 << LayerMask.NameToLayer("Projectile")))
				{
					inputDown = false;
					SelectProjectile(hit.collider.gameObject);
					AimLineState = Data.Input.AimLineState.ProjectileSelected;
				}
			}
			else if (InputSystem == 1)
			{
				if (!hasLastMouseDownPosition)
				{
					hasLastMouseDownPosition = true;
					LastMouseDownPosition = MousePosition;
					TouchDownSphere.Instance.Set(new Vector3(
						MousePosition.x,
						MousePosition.y,
						Data.PositionLayer.Projectile));
				}
				if (TouchDownSphere.Instance.HasSelectedAProjectile)
				{
					SelectProjectile(TouchDownSphere.Instance.SelectedProjectile);
					AimLineState = Data.Input.AimLineState.ProjectileSelected;
				}
			}
			else if (InputSystem == 2)
			{
				if (ProjectileHandler.Instance.ActiveProjectiles.Count > 0)
				{
					if (!hasLastMouseDownPosition)
					{
						hasLastMouseDownPosition = true;
						SelectProjectile(FindClosestProjectile());
						LastMouseDownPosition = SelectedProjectile.transform.position;
						TouchDownSphere.Instance.Set(new Vector3(
							LastMouseDownPosition.x,
							LastMouseDownPosition.y,
							Data.PositionLayer.Projectile));
						AimLineState = Data.Input.AimLineState.ProjectileSelected;
					}
				}
			}
            else if (InputSystem == 3)
            {
                if (ProjectileHandler.Instance.ActiveProjectiles.Count > 0)
                {
                    if (!hasLastMouseDownPosition)
                    {
                        hasLastMouseDownPosition = true;
                        SelectProjectile(FindClosestProjectile());
                        LastMouseDownPosition = SelectedProjectile.transform.position;
                        TouchDownSphere.Instance.Set(new Vector3(
                            LastMouseDownPosition.x,
                            LastMouseDownPosition.y,
                            Data.PositionLayer.Projectile));
                        AimLineState = Data.Input.AimLineState.ProjectileSelected;
                    }
                }
            }
		}
	}
	
	private void SelectProjectile(GameObject projectile)
	{
		SelectedProjectile = projectile;
		projectile.GetComponent<Projectile>().Select();
	}
	
	private GameObject FindClosestProjectile()
	{
		float closestDistance = 100000f;
		GameObject closestProjectile = null;
		foreach (Projectile projectile in ProjectileHandler.Instance.ActiveProjectiles)
		{
			float currentDistance = Vector3.Distance(projectile.transform.position, MousePosition);
			if (currentDistance < closestDistance)
			{
				closestDistance = currentDistance;
				closestProjectile = projectile.gameObject;
			}
		}
		return closestProjectile;
	}
    
    private GameObject FindClosestEnemy()
    {

        float closestDistance = 100000f;
        GameObject closestEnemy = null;
        foreach (EnemyGroup enemyGroup in EnemyHandler.Instance.EnemyGroups)
        {
            foreach (Enemy enemy in enemyGroup.Enemies)
            {
                float currentDistance = Vector3.Distance(enemy.transform.position, MousePosition);
                if (currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    closestEnemy = enemy.gameObject;
                }
            }
        }
        return closestEnemy;
    }
	
	private void HandleInputUp()
	{
		if (inputUp)
		{			
			if (SelectedProjectile != null)
			{				
				Vector3 direction = Vector3.zero;
                if (InputSystem == 2) 
                {
                    direction = Vector3.Normalize(new Vector3(
					MousePosition.x - SelectedProjectile.transform.position.x,
					MousePosition.y - SelectedProjectile.transform.position.y,
					0f));
                }
                else if (InputSystem == 3)
                {
                    direction = Vector3.Normalize(new Vector3(
                    SelectedEnemy.transform.position.x - SelectedProjectile.transform.position.x,
                    SelectedEnemy.transform.position.y - SelectedProjectile.transform.position.y,
                    0f));
                }
                SelectedProjectile.GetComponent<Projectile>().Release(direction, Data.Input.StaticReleaseSpeed);
                if (InputSystem == 3)
                {
                    SelectedProjectile.GetComponent<Projectile>().TargetID = SelectedEnemy.GetComponent<Enemy>().ID;
                    SelectedProjectile.transform.position = SelectedEnemy.transform.position - direction * 0.7f;
                }
                SelectedProjectile = null;
			}
			inputUp = false;
			hasLastMouseDownPosition = false;
			inputDown = false;
			AimLineState = Data.Input.AimLineState.None;
			TouchDownSphere.Instance.MoveToStandyByPosition();
		}
	}

    private void HitEnemyWithProjectile(Enemy enemy, Projectile projectile)
    {

    }

    private void UpdateInputSystem()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            InputSystem = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            InputSystem = 3;
        }
    }
}

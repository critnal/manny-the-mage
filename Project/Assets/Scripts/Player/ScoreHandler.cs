﻿using UnityEngine;
using System.Collections;

public class ScoreHandler : Singleton<ScoreHandler> 
{	
	protected ScoreHandler() {}

    public float PlayerScore { get; private set; }
	public float PlayerComboPoints { get; private set; }
    private float ComboExpireTime = 0f;
	
	private void Awake()
	{
		PlayerScore = 0;
        PlayerComboPoints = 1;
	}
	
	public void PlayerKilledEnemy(Enemy enemy)
	{
        float score = enemy.Points * PlayerComboPoints;
		PlayerScore += score;
		FloatingText.Instance.NewLabel(score.ToString(), enemy.transform.position);
        PlayerComboPoints++;
        PlayerComboPoints = Mathf.Min(PlayerComboPoints, Data.Score.MaxComboPoints);
        ComboExpireTime = Time.time + Data.Score.ComboDuration;
	}
	
	private void Update()
	{
		if (PlayerComboPoints > 1 && Time.time > ComboExpireTime)
		{
            PlayerComboPoints = 1;
		}
	}
	
	public void StartGame()
	{
		PlayerScore = 0;
		PlayerComboPoints = 1;
	}
}

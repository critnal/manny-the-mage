﻿using UnityEngine;
using System.Collections;

public class PlayerScore : Singleton<PlayerScore> 
{
	protected PlayerScore() {}
	
    //private Texture2D barBackground;
	public GUIStyle ScoreStyle;
	public GUIStyle ComboPointStyle;
	
	private void Awake()
	{
        //barBackground = (Texture2D)Resources.Load("Visuals/Textures/HealthBarBackground");
		ScoreStyle.normal.textColor = new Color(255, 236, 0, 1);
		ScoreStyle.fontStyle = FontStyle.Bold;
		ScoreStyle.fontSize = Screen.height / 20;
		ScoreStyle.alignment = TextAnchor.MiddleRight;
		ComboPointStyle.normal.textColor = new Color(255, 236, 0, 1);
		ComboPointStyle.fontStyle = FontStyle.Bold;
		ComboPointStyle.fontSize = Screen.height / 20;
		ComboPointStyle.alignment = TextAnchor.MiddleLeft;
	}
	
	private void OnGUI()
	{
//		Rect barRect = new Rect(0, 0, Screen.width, Screen.height / 20);
//		GUI.DrawTexture(barRect, barBackground, ScaleMode.StretchToFill);
        if (GameManager.Instance.State == GameManager.GameState.Play)
        {
            Rect scoreRect = new Rect(Screen.width * 5 / 10, 0, Screen.width * 5 / 10, Screen.height / 20);
            GUI.Label(scoreRect, ScoreHandler.Instance.PlayerScore.ToString(), ScoreStyle);
            Rect comboPointRect = new Rect(0, 0, Screen.width * 5 / 10, Screen.height / 20);
            string comboPoints = ScoreHandler.Instance.PlayerComboPoints.ToString("0") + "x";
            GUI.Label(comboPointRect, comboPoints, ComboPointStyle);
        }
	}
}

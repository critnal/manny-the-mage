﻿using UnityEngine;
using System.Collections;

public class PlayerBase : Singleton<PlayerBase> 
{
	protected PlayerBase() {}	
	
	private Texture2D healthBarGreen;
	private Texture2D healthBarRed;
	private Texture2D healthBarOrange;
	
	private void Awake() 
	{
		healthBarGreen = (Texture2D)Resources.Load("Visuals/Textures/HealthBarGreen");
		healthBarRed = (Texture2D)Resources.Load("Visuals/Textures/healthBarRed");
		healthBarOrange = (Texture2D)Resources.Load("Visuals/Textures/healthBarOrange");
	}
	
	private void OnGUI()
	{
//		Rect baseRect = new Rect(0, Screen.height * 19/20, Screen.width, Screen.height * 1/20);
        if (GameManager.Instance.State == GameManager.GameState.Play)
        {
            GUI.DrawTexture(new Rect(0, Screen.height * 39 / 40, Screen.width * GetHealthPercentage(), Screen.height * 1 / 40), GetHealthColour(), ScaleMode.StretchToFill);
        }
	}
	
	private Texture2D GetHealthColour()
	{
		if (GetHealthPercentage() > 0.75f)
		{
			return healthBarGreen;
		}
		else if (GetHealthPercentage() > 0.35f)
		{
			return healthBarOrange;
		}
		else 
		{
			return healthBarRed;
		}
	}
	
	private float GetHealthPercentage()
	{
		return BaseHandler.Instance.Health / BaseHandler.Instance.MaxHealth;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloatingText : Singleton<FloatingText> 
{
	protected FloatingText() {}
	
	public List<FloatingLabel> FloatingLabels { get; private set; }
	
	public GUIStyle FloatingStyle;
	
	private void Awake()
	{
		FloatingLabels = new List<FloatingLabel>();
		FloatingStyle.normal.textColor = new Color(255, 236, 0, 1);
		FloatingStyle.fontStyle = FontStyle.Bold;
		FloatingStyle.fontSize = Mathf.RoundToInt(Screen.height / 50f);
	}
	
	public void NewLabel(string text, Vector3 position)
	{
		FloatingLabels.Add(new FloatingLabel(position, text));
	}
	
	private void OnGUI()
	{
		Color originalColour = GUI.color;
		for (int i = FloatingLabels.Count - 1; i >= 0; i--)
		{
			FloatingLabels[i].Scroll();
			Vector3 screenPoint = Camera.main.WorldToScreenPoint(FloatingLabels[i].Position);
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, FloatingLabels[i].Alpha);
			FloatingStyle.fontSize = Screen.height / 25;
			string text = "+" + FloatingLabels[i].Text.ToString();
			GUI.Label(new Rect(screenPoint.x, Screen.height - screenPoint.y, 100f, 100f), text, FloatingStyle);
			if (FloatingLabels[i].Alpha < 0)
			{
				FloatingLabels.RemoveAt(i);
			}
			else
			{
				FloatingLabels[i].Fade();
			}
		}
		GUI.color = originalColour;
	}
	
	public class FloatingLabel
	{
		public Vector3 Position { get; set; }
		public string Text { get; set; }
		public float Alpha { get; set; }
		
		public FloatingLabel(Vector3 position, string text)
		{
			this.Position = position;
			this.Text = text;
			this.Alpha = 1.0f;
		}
		
		public void Scroll()
		{
			Position = new Vector3(Position.x, Position.y + Time.deltaTime * Data.Score.PlusScoreLabelScrollSpeed, Position.z);
		}
		
		public void Fade()
		{
			Alpha -= Time.deltaTime / Data.Score.PlusScoreLabelDuration;
		}
	}
}

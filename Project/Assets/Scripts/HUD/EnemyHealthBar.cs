﻿using UnityEngine;
using System.Collections;

public class EnemyHealthBar : MonoBehaviour 
{
	private Enemy me;
	private float currentHealth = 0f;
	private float maxHealth = 0f;
	private float visibleHealth = 0f;
	private float previousHealth = 0f;
	public float slideDuration = 0.5f;
	public float healthBarWidthFraction = 0.05f;
	public float healthBarHeightFraction = 0.01f;
	public float healthBarOffsetFraction = 0.05f;
	public float healthBarBorderRatio = 0.1f;
	private Texture2D healthBarGreen;
	private Texture2D healthBarRed;
	private Texture2D healthBarOrange;
    //private Texture2D healthBarBackground;
	
	private void Awake() 
	{
		me = this.GetComponent<Enemy>();
		currentHealth = me.CurrentHealth;
		maxHealth = me.MaxHealth;
		visibleHealth = currentHealth;
		previousHealth = currentHealth;
		healthBarGreen = (Texture2D)Resources.Load("Visuals/Textures/HealthBarGreen");
		healthBarRed = (Texture2D)Resources.Load("Visuals/Textures/healthBarRed");
		healthBarOrange = (Texture2D)Resources.Load("Visuals/Textures/healthBarOrange");
        //healthBarBackground = (Texture2D)Resources.Load("Visuals/Textures/healthBarBackground");
	}
	
	private void Update() 
	{		
		currentHealth = me.CurrentHealth;
		if (visibleHealth > currentHealth)
		{
			visibleHealth -= (previousHealth - currentHealth) * Time.deltaTime / slideDuration;
		}
		else
		{
			previousHealth = currentHealth;
		}
	}
	
	private void OnGUI()
	{		
		Vector3 myPosition = Camera.main.WorldToScreenPoint(this.transform.position);
		float healthBarWidth = Screen.height * healthBarWidthFraction;
		float healthBarHeight = Screen.height * healthBarHeightFraction;
		float healthBarLeft = myPosition.x - healthBarWidth / 2;
		float healthBarTop = Screen.height - myPosition.y - healthBarHeight - Screen.height * healthBarOffsetFraction;
		Texture2D currentHealthTexture = GetHealthColour();
		
		Rect healthBarRect = new Rect(healthBarLeft, healthBarTop, healthBarWidth, healthBarHeight);
		GUI.BeginGroup(healthBarRect);
		{
			GUI.DrawTexture(new Rect(0, 0, healthBarRect.width * GetHealthPercentage(), healthBarRect.height), currentHealthTexture);
		}
		GUI.EndGroup();
	}
	
	private Texture2D GetHealthColour()
	{
		if (GetHealthPercentage() > 0.75f)
		{
			return healthBarGreen;
		}
		else if (GetHealthPercentage() > 0.35f)
		{
			return healthBarOrange;
		}
		else 
		{
			return healthBarRed;
		}
	}
	
	private float GetHealthPercentage()
	{
		return currentHealth / maxHealth;
	}
}

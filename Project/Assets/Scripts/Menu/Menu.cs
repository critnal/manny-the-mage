﻿using UnityEngine;
using System.Collections;

public class Menu : Singleton<Menu> 
{

    private void OnGUI()
    {
        if (GameManager.Instance.State == GameManager.GameState.Menu)
        {
            float screenWidth = Screen.width;
            float screenHeight = Screen.height;
            Rect mainRect = new Rect(screenWidth / 4, screenHeight / 4, screenWidth / 2, screenHeight / 2);
            GUI.BeginGroup(mainRect);
            {
                Rect startButtonRect = new Rect(0, 0, mainRect.width, mainRect.height);
                if (GUI.Button(startButtonRect, "Start"))
                {
                    GameManager.Instance.StartGame();
                }
            }
            GUI.EndGroup();
        }
    }
}
